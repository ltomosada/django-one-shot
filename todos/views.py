from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.shortcuts import render, redirect
from django.urls import reverse_lazy


from todos.models import TodoItem, TodoList


# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetail(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreate(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def form_valid(self, form):
        plan = form.save(commit=True)
        return redirect("todo_list_detail", pk=plan.id)


class TodoListUpdate(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def form_valid(self, form):
        plan = form.save(commit=True)
        return redirect("todo_list_detail", pk=plan.id)


class TodoListDelete(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreate(CreateView):
    model = TodoItem
    template_name = "items/item_create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdate(UpdateView):
    model = TodoItem
    template_name = "items/item_edit.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
